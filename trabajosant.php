<?php
    include "cabecera1.php";
?>

    <article class="galeria">

        <div class="container1">
            
            <ul class="slider1" id="display">

                <li id="slide1">
                    <img src="imagenes/tant/falcon.jpg" alt="Falcon">
                </li>
                <li id="slide2">
                    <img src="imagenes/tant/falcon1.jpg" alt="Falcon">
                </li>
                <li id="slide3">
                    <img src="imagenes/tant/falcon2.jpg" alt="Falcon">
                </li>
                <li id="slide4">
                    <img src="imagenes/tant/hilux.jpg" alt="Hilux">
                </li>
                <li id="slide5">
                    <img src="imagenes/tant/hilux1.jpg" alt="Hilux">
                </li>
                <li id="slide6">
                    <img src="imagenes/tant/hilux2.jpg" alt="Hilux">
                </li>
                <li id="slide7">
                    <img src="imagenes/tant/hilux3.jpg" alt="Hilux">
                </li>
                <li id="slide8">
                    <img src="imagenes/tant/hilux4.jpg" alt="Hilux">
                </li>
                <li id="slide9">
                    <img src="imagenes/tant/hilux5.jpg" alt="Hilux">
                </li>
                <li id="slide10">
                    <img src="imagenes/tant/hilux6.jpg" alt="Hilux">
                </li>
                <li id="slide11">
                    <img src="imagenes/tant/hilux7.jpg" alt="Hilux">
                </li>
                <li id="slide12">
                    <img src="imagenes/tant/gol.jpg" alt="Gol">
                </li>
                <li id="slide13">
                    <img src="imagenes/tant/gol1.jpg" alt="Gol">
                </li>
                <li id="slide14">
                    <img src="imagenes/tant/gol2.jpg" alt="Gol">
                </li>
                <li id="slide15">
                    <img src="imagenes/tant/nissan.jpg" alt="??">
                </li>
                <li id="slide16">
                    <img src="imagenes/tant/nissan1.jpg" alt="??">
                </li>
                <li id="slide17">
                    <img src="imagenes/tant/blanco.jpg" alt="??">
                </li>
                <li id="slide18">
                    <img src="imagenes/tant/blanco1.jpg" alt="??">
                </li>
                <li id="slide19">
                    <img src="imagenes/tant/blanco2.jpg" alt="??">
                </li>
                <li id="slide20">
                    <img src="imagenes/tant/blanco4.jpg" alt="??">
                </li>
                <li id="slide21">
                    <img src="imagenes/tant/blanco5.jpg" alt="??">
                </li>
                <li id="slide22">
                    <img src="imagenes/tant/blanco6.jpg" alt="??">
                </li>
                <li id="slide23">
                    <img src="imagenes/tant/gol3.jpg" alt="gol">
                </li>
                <li id="slide24">
                    <img src="imagenes/tant/gol4.jpg" alt="gol">
                </li>
                <li id="slide25">
                    <img src="imagenes/tant/gris.jpg" alt="??">
                </li>
                <li id="slide26">
                    <img src="imagenes/tant/gris1.jpg" alt="??">
                </li>
                <li id="slide27">
                    <img src="imagenes/tant/gris2.jpg" alt="??">
                </li>
                <li id="slide28">
                    <img src="imagenes/tant/gris3.jpg" alt="??">
                </li>
                <li id="slide29">
                    <img src="imagenes/tant/gris4.jpg" alt="??">
                </li>
                <li id="slide30">
                    <img src="imagenes/tant/toyota.jpg" alt="toyota">
                </li>
                <li id="slide31">
                    <img src="imagenes/tant/toyota1.jpg" alt="toyota">
                </li>
                <li id="slide32">
                    <img src="imagenes/tant/fiat47.jpg" alt="toyota">
                </li>
                <li id="slide33">
                    <img src="imagenes/tant/fiat471.jpg" alt="toyota">
                </li>
                <li id="slide34">
                    <img src="imagenes/tant/fiat473.jpg" alt="toyota">
                </li>
                <li id="slide35">
                    <img src="imagenes/tant/fiat474.jpg" alt="toyota">
                </li>
                <li id="slide36">
                    <img src="imagenes/tant/fiat475.jpg" alt="toyota">
                </li>
                <li id="slide37">
                    <img src="imagenes/tant/fiat476.jpg" alt="toyota">
                </li>
                <li id="slide38">
                    <img src="imagenes/tant/fiat.jpg" alt="toyota">
                </li>
                <li id="slide39">
                    <img src="imagenes/tant/fiat1.jpg" alt="toyota">
                </li>
                <li id="slide40">
                    <img src="imagenes/tant/fiat2.jpg" alt="toyota">
                </li>
                <li id="slide41">
                    <img src="imagenes/tant/fiat3.jpg" alt="toyota">
                </li>
                <li id="slide42">
                    <img src="imagenes/tant/fiat4.jpg" alt="toyota">
                </li>
                <li id="slide43">
                    <img src="imagenes/tant/fiat5.jpg" alt="toyota">
                </li>
            </ul>   

            <ul class="menu1">
                <p class="txt2">¡Tocá las imágenes de abajo para agrandarlas!</p>
            </ul>

            <section class="autos">
                
                <div class="galcontenedor">
                    <a class="autocolumna" href="#slide1">
                        <img src="imagenes/tant/falcon.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide2">
                        <img src="imagenes/tant/falcon1.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide3">
                        <img src="imagenes/tant/falcon2.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide4">
                        <img src="imagenes/tant/hilux.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide5">
                        <img src="imagenes/tant/hilux1.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide6">
                        <img src="imagenes/tant/hilux2.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide7">
                        <img src="imagenes/tant/hilux3.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide8">
                        <img src="imagenes/tant/hilux4.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide9">
                        <img src="imagenes/tant/hilux5.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide10">
                        <img src="imagenes/tant/hilux6.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide11">
                        <img src="imagenes/tant/hilux7.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide12">
                        <img src="imagenes/tant/gol.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide13">
                        <img src="imagenes/tant/gol1.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide14">
                        <img src="imagenes/tant/gol2.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide15">
                        <img src="imagenes/tant/nissan.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide16">
                        <img src="imagenes/tant/nissan1.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide17">
                        <img src="imagenes/tant/blanco.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide18">
                        <img src="imagenes/tant/blanco1.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide19">
                        <img src="imagenes/tant/blanco2.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide20">
                        <img src="imagenes/tant/blanco4.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide21">
                        <img src="imagenes/tant/blanco5.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide22">
                        <img src="imagenes/tant/blanco6.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide23">
                        <img src="imagenes/tant/gol3.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide24">
                        <img src="imagenes/tant/gol4.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide25">
                        <img src="imagenes/tant/gris.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide26">
                        <img src="imagenes/tant/gris1.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide27">
                        <img src="imagenes/tant/gris2.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide28">
                        <img src="imagenes/tant/gris3.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide29">
                        <img src="imagenes/tant/gris4.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide30">
                        <img src="imagenes/tant/toyota.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide31">
                        <img src="imagenes/tant/toyota1.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide32">
                        <img src="imagenes/tant/fiat47.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide33">
                        <img src="imagenes/tant/fiat471.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide34">
                        <img src="imagenes/tant/fiat473.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide35">
                        <img src="imagenes/tant/fiat474.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide36">
                        <img src="imagenes/tant/fiat475.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide37">
                        <img src="imagenes/tant/fiat476.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide38">
                        <img src="imagenes/tant/fiat.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide39">
                        <img src="imagenes/tant/fiat1.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide40">
                        <img src="imagenes/tant/fiat2.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide41">
                        <img src="imagenes/tant/fiat3.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide42">
                        <img src="imagenes/tant/fiat4.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide43">
                        <img src="imagenes/tant/fiat5.jpg" class="galimg">
                    </a>
                </div>

            </section>
            
        </div>

    </article>

    

<?php
    include "pie.php";
?>