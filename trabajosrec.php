<?php
    include "cabecera1.php";
?>

    <article class="galeria">

        <div class="container1">
            
            <ul class="slider1" id="display">
                <li id="slide-16">
                    <img src="imagenes/trec/chevrolet19281.jpg" alt="Fiat">
                </li>
                <li id="slide-15">
                    <img src="imagenes/trec/chevrolet1928.jpg" alt="Fiat">
                </li>
                <li id="slide-14">
                    <img src="imagenes/trec/camionetahonda5.jpg" alt="Fiat">
                </li>
                <li id="slide-13">
                    <img src="imagenes/trec/camionetahonda4.jpg" alt="Fiat">
                </li>
                <li id="slide-12">
                    <img src="imagenes/trec/camionetahonda3.jpg" alt="Fiat">
                </li>
                <li id="slide-11">
                    <img src="imagenes/trec/camionetahonda2.jpg" alt="Fiat">
                </li>
                <li id="slide-10">
                    <img src="imagenes/trec/camionetahonda1.jpg" alt="Fiat">
                </li>
                <li id="slide-9">
                    <img src="imagenes/trec/camionetahonda.jpg" alt="Fiat">
                </li>
                <li id="slide-8">
                    <img src="imagenes/trec/paragolpegris8.jpg" alt="Fiat">
                </li>
                <li id="slide-7">
                    <img src="imagenes/trec/paragolpegris7.jpg" alt="Fiat">
                </li>
                <li id="slide-6">
                    <img src="imagenes/trec/paragolpegris6.jpg" alt="Fiat">
                </li>
                <li id="slide-5">
                    <img src="imagenes/trec/paragolpegris5.jpg" alt="Fiat">
                </li>
                <li id="slide-4">
                    <img src="imagenes/trec/paragolpegris4.jpg" alt="Fiat">
                </li>
                <li id="slide-3">
                    <img src="imagenes/trec/paragolpegris3.jpg" alt="Fiat">
                </li>
                <li id="slide-2">
                    <img src="imagenes/trec/paragolpegris2.jpg" alt="Fiat">
                </li>
                <li id="slide-1">
                    <img src="imagenes/trec/paragolpegris1.jpg" alt="Fiat">
                </li>
                <li id="slide0">
                    <img src="imagenes/trec/paragolpegris.jpg" alt="Fiat">
                </li>
                <li id="slide1">
                    <img src="imagenes/trec/fiat.jpg" alt="Fiat">
                </li>
                <li id="slide2">
                    <img src="imagenes/trec/fiat1.jpg" alt="Fiat">
                </li>
                <li id="slide3">
                    <img src="imagenes/trec/fiat2.jpg" alt="Fiat">
                </li>
                <li id="slide4">
                    <img src="imagenes/trec/gol.jpg" alt="Gol">
                </li>
                <li id="slide5">
                    <img src="imagenes/trec/gol1.jpg" alt="Gol">
                </li>
                <li id="slide6">
                    <img src="imagenes/trec/gol2.jpg" alt="Gol">
                </li>
                <li id="slide7">
                    <img src="imagenes/trec/gol3.jpg" alt="Gol">
                </li>
                <li id="slide8">
                    <img src="imagenes/trec/gol4.jpg" alt="Gol">
                </li>
                <li id="slide9">
                    <img src="imagenes/trec/gol5.jpg" alt="Gol">
                </li>
                <li id="slide10">
                    <img src="imagenes/trec/gol6.jpg" alt="Gol">
                </li>
                <li id="slide11">
                    <img src="imagenes/trec/gol7.jpg" alt="Gol">
                </li>
                <li id="slide12">
                    <img src="imagenes/trec/golgris.jpg" alt="Gol">
                </li>
                <li id="slide13">
                    <img src="imagenes/trec/golgris1.jpg" alt="Gol">
                </li>
                <li id="slide14">
                    <img src="imagenes/trec/golgris2.jpg" alt="Gol">
                </li>
                <li id="slide15">
                    <img src="imagenes/trec/paragolpe.jpg" alt="??">
                </li>
                <li id="slide16">
                    <img src="imagenes/trec/paragolpe1.jpg" alt="??">
                </li>
                <li id="slide17">
                    <img src="imagenes/trec/paragolpe2.jpg" alt="??">
                </li>
                <li id="slide18">
                    <img src="imagenes/trec/paragolpe3.jpg" alt="??">
                </li>
            </ul>   

            <ul class="menu1">
                <p class="txt2">¡Tocá las imágenes de abajo para agrandarlas!</p>
            </ul>

            <section class="autos">
                
                <div class="galcontenedor">
                    <a class="autocolumna" href="#slide-16">
                        <img src="imagenes/trec/chevrolet19281.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide-15">
                        <img src="imagenes/trec/chevrolet1928.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide-14">
                        <img src="imagenes/trec/camionetahonda5.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide-13">
                        <img src="imagenes/trec/camionetahonda4.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide-12">
                        <img src="imagenes/trec/camionetahonda3.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide-11">
                        <img src="imagenes/trec/camionetahonda2.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide-10">
                        <img src="imagenes/trec/camionetahonda1.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide-9">
                        <img src="imagenes/trec/camionetahonda.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide-8">
                        <img src="imagenes/trec/paragolpegris8.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide-7">
                        <img src="imagenes/trec/paragolpegris7.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide-6">
                        <img src="imagenes/trec/paragolpegris6.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide-5">
                        <img src="imagenes/trec/paragolpegris5.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide-4">
                        <img src="imagenes/trec/paragolpegris4.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide-3">
                        <img src="imagenes/trec/paragolpegris3.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide-2">
                        <img src="imagenes/trec/paragolpegris2.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide-1">
                        <img src="imagenes/trec/paragolpegris1.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide0">
                        <img src="imagenes/trec/paragolpegris.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide1">
                        <img src="imagenes/trec/fiat.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide2">
                        <img src="imagenes/trec/fiat1.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide3">
                        <img src="imagenes/trec/fiat2.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide4">
                        <img src="imagenes/trec/gol.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide5">
                        <img src="imagenes/trec/gol1.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide6">
                        <img src="imagenes/trec/gol2.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide7">
                        <img src="imagenes/trec/gol3.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide8">
                        <img src="imagenes/trec/gol4.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide9">
                        <img src="imagenes/trec/gol5.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide10">
                        <img src="imagenes/trec/gol6.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide11">
                        <img src="imagenes/trec/gol7.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide12">
                        <img src="imagenes/trec/golgris.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide13">
                        <img src="imagenes/trec/golgris1.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide14">
                        <img src="imagenes/trec/golgris2.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide15">
                        <img src="imagenes/trec/paragolpe.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide16">
                        <img src="imagenes/trec/paragolpe1.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide17">
                        <img src="imagenes/trec/paragolpe2.jpg" class="galimg">
                    </a>
                    <a class="autocolumna" href="#slide18">
                        <img src="imagenes/trec/paragolpe3.jpg" class="galimg">
                    </a>
                </div>

            </section>
            
        </div>

    </article>

<?php
    include "pie.php";
?>