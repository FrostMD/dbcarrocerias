<?php

    include "cabecera.php";

?>

<article class="articleindex">

  <div class="container">

    <ul class="slider">
      <li id="slide1">
        <img src="imagenes/1.jpg" alt="dodge">
      </li>
      <li id="slide2">
        <img src="imagenes/2.jpg" alt="gol">
      </li>
      <li id="slide3">
        <img src="imagenes/3.jpg" alt="honda">
      </li>
      <li id="slide4">
        <img src="imagenes/4.jpg" alt="honda">
      </li>
      <li id="slide5">
        <img src="imagenes/5.jpg" alt="cronos">
      </li>
      <li id="slide6">
        <img src="imagenes/imgver.png" alt="miracle ropits">
      </li>
      <li id="slide7">
        <img src="imagenes/imgver.png" alt="miracle ropits">
      </li>
      <li id="slide8">
        <img src="imagenes/imgver.png" alt="miracle ropits">
      </li>
      <li id="slide9">
        <img src="imagenes/imgver.png" alt="miracle ropits">
      </li>
    </ul>

    <ul class="menu">
        <li>
          <a href="#slide1">1</a>
        </li>
        <li>
          <a href="#slide2">2</a>
        </li>
        <li>
          <a href="#slide3">3</a>
        </li>
        <li>
          <a href="#slide4">4</a>
        </li>
        <li>
          <a href="#slide5">5</a>
        </li>
        <li>
          <a href="#slide6">6</a>
        </li>
        <li>
          <a href="#slide7">7</a>
        </li>
        <li>
          <a href="#slide8">8</a>
        </li>
        <li>
          <a href="#slide9">9</a>
        </li>
    </ul>

  </div>

</article>

<article class="articleindex2">

  <p class="subtxt">¿Quiénes somos?<br></p>
  <p class="txt">Somos De Bruno Carrocerias, un taller dedicado a la chapa, pintura, lustrado, y limpieza. Trabajamos con vehículos, motos, máquinas, electrodomésticos, y más. Contamos con una experiencia de mas de 30 años en el rubro.
  Trabajamos para concesionarias y particulares.<br> Si buscas contactarnos, encontranos en los medios alojados en la parte inferior de la página.</p>

</article>


<?php

     include "pie.php";

?>