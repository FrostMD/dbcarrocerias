<!DOCTYPE html>
<html lang="es">
<head>

    <meta charset="UTF-8">
    <meta name="keyboard" content="autos, chapa, pintura">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>De Bruno Carrocerias</title>

    <link rel="stylesheet" type="text/css" href="css/galeria.css">
    <link rel="stylesheet" type="text/css" href="css/txt.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">

</head>

<body class="body1">

    <div class="contenido1">

    <header>
            
            <nav class="nav">

                <img src="imagenes/dbcarrocerias.png" class="logo"> <!--Podriamos decir que el logo si queremos que vaya del lado derecho, deberia estar el html abajo de los "li" del nav. Pero la posicion de este se modifica de mejor manera via CSS.-->
                <ul>
                    <li><a href="index.php">Inicio</a></li>
                    <li><a href="trabajosrec.php">Trabajos recientes</a></li>
                    <li><a href="trabajosant.php">Trabajos antiguos</a></li>
                </ul>
            
            </nav>

    </header>

    